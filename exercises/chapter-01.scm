;; ;; Example 1.1.7 Square Roots by Newton's Method
;; (define (square x)
;;   (* x x))

;; (define (average x y)
;;   (/ (+ x y) 2))

;; (define (improve guess x)
;;   (average guess (/ x guess)))

;; (define (good-enough? guess x)
;;   (< (abs (- (square guess) x)) 0.001))

;; (define (sqrt-iter guess x)
;;   (if (good-enough? guess x)
;;       guess
;;       (sqrt-iter (improve guess x) x)))

;; (define (sqrt x)
;;   (sqrt-iter 1.0 x))

;; ;; Exos 1.6-------------------------------------------------------------------
;; (define (new-if predicate then-clause else-clause)
;;   (cond (predicate then-clause)
;;         (else else-clause)))

;; (define (new-sqrt-iter guess x)
;;     (new-if (good-enough? guess x)
;;             guess
;;             (new-sqrt-iter (improve guess x) x)))
;; ;Answer: 
;; ; Scheme evaluates all the arguments to a function before doing
;; ; anything else, so if you try to use `new-if` recursively, it
;; ; will drop into a new form of itself as soon as it hits evaluation
;; ; without the possibility of choosing a branch and 
;; ; terminating the recursion.

;; (define (inc a)
;;   (+ a 1))
;; (define (dec a)
;;   (- a 1))



;; (define (new-sqrt x)
;;   (new-sqrt-iter 1.0 x))

;; ;; Exos 1.9
;; ; The first implementation is a recursive process, the second is only
;; ; a recursive procedure.
;; (define (plus-one a b)
;;   (if (= a 0) b (inc (plus-one (dec a) b))))


;; (define (plus-two a b)
;;   (if (= a 0) b (plus-two (dec a) (inc b))))

;; ;; Exos 1.10
;; (define (ackerman x y)
;;   (cond ((= y 0) 0)
;;         ((= x 0)(* 2 y))
;;         ((= y 1) 2)
;;         (else (ackerman (- x 1) (ackerman x (- y 1))))))

;; ;(define (f n) (ackerman 0 n)) ;; 2*n
;; ;(define (g n) (ackerman 1 n)) ;; 2**n
;; ;(define (h n) (ackerman 2 n)) ;; reduce pow 2 [2 for i in range(5)] (something to do with up-arrow notation)

;; ;; Exos 1.11
;; ; recursive:
;; (define (f n)
;;   (if (< n 3)
;;       n
;;       (+ (f (- n 1)) (* 2 (f (- n 2))) (* 3 (f (- n 3))))))
;; ; iterative

;; (define (iterative-f n)
;;   (define (f-iter target-n current-n f-n f-n-1 f-n-2)
;;     (if (= target-n current-n)
;;         f-n
;;         (f-iter target-n (+ current-n 1) (+ f-n (* 2 f-n-1) (* 3 f-n-2)) f-n f-n-1)))
;;   (cond
;;     ((= n 1) 1)
;;     ((= n 2) 2)
;;     (else (f-iter n 2 2 1 0))))


;; ;; Exos 1.12
;; ; Create pascal's triangle using iterative processes
;; (define (next-row row)
;;   (map + (cons 0 row) (append row '(0))))

;; (define (pascal n)
;;   (define (pascal-iter target-n current-n rows)
;;     (if (= target-n current-n)
;;         rows
;;         (pascal-iter target-n (+ current-n 1) (cons (next-row (first rows)) rows))))
;;   (pascal-iter n 0 '((1))))

;; ;; Exos 1.13
;; ; Skipped. It's too mathy, and I'm pretty sure I did this in Algebra 1.

;; ;; Exos 1.14
;; ;; Draw the tree illustrating the process generated by the
;; ;; count-change procedure of 1.2.2 in making change for 11 cents. What
;; ;; are the orders of growth of the space and number of steps used by
;; ;; this process as the amount to be changed increases?

;; (define (first-denomination kind-of-coin)
;;   (cond ((= kind-of-coin 1) 1)
;;         ((= kind-of-coin 2) 5)
;;         ((= kind-of-coin 3) 10)
;;         ((= kind-of-coin 4) 25)
;;         ((= kind-of-coin 5) 50)))

;; (define (cc amount kinds-of-coins)
;;   (cond ((= amount 0) 1)
;;         ((or (< amount 0)
;;              (= kinds-of-coins 0))
;;          0)
;;         (else
;;          (+ (cc amount (- kinds-of-coins 1))
;;             (cc (- amount (first-denomination kinds-of-coins))
;;                 kinds-of-coins)))))

;; (define (count-change amount)
;;   (cc amount 5))


;; ;;Illustration
;; (count-change 11)
;; (cc 11 5)
;; (+ (cc 11 4)
;;    (cc -39 5))
;; (+ (+ (cc 11 3)
;;       (cc -14 4))
;;    0)
;; (+ (+ (+ (cc 11 2)
;;          (cc 1 3))
;;       0)
;;    0)
;; (+ (+ (+ (+ (cc 11 1)
;;             (cc 6 2))
;;          (+ (cc 1 2)
;;             (cc -4 3)))
;;       0)
;;    0)
;; (+ (+ (+ (+ (+ (cc 11 0)
;;                (cc 10 1))
;;             (+ (cc 6 1)
;;                (cc 1 1)))
;;          (+ (+ (cc 1 1)
;;                (cc -4 2))
;;             0))
;;       0)
;;    0)
;; (+ (+ (+ (+ (+ 0
;;                1)
;;             (+ 1
;;                1)))
;;       (+ (+ 1
;;             0))
;;       0)
;;    0)
;; = 4

;; Exos 1.15
;; 1.) Each application reduces the arg by a factor of 1/3, so five
;; applications are needed
;;(/ 12.15 3) ;;4.05
;;(/ (/ 12.15 3) 3) ;; 1.34999999999
;;(/ (/ (/ 12.15 3) 3) 3);; 4.499999999999
;;(/ (/ (/ (/ 12.15 3) 3) 3) 3) ;; 0.15
;;(/ (/ (/ (/ (/ 12.15 3) 3) 3) 3) 3) ;; 0.05

;; 2.) Logarithmic: We require `n` applications of `sine`, where
;;              a * (1/3)**n < 0.1
;; which is equivalent to
;;              log(a) + n * log(1/3)  < log(0.1)
;;              n > (log(0.1) - log(a)) / log(1/3)


;; Exos 1.16
;; Design an iterative, logarithmic time exponentiation algorithm
;; which uses successive squaring and a state variable.

(define (my-exp b n)
  (iter-exp b n 1))

(define (iter-exp b n a)
  (define (even? x) (= (remainder x 2) 0))
  (cond ((= n 1) (* a b))
        ((= n 0) a)
        (else
         (if (even? n)
             (iter-exp (* b b) (/ n 2) a) ;; (b**2)**(n/2) = (b**n)
             (iter-exp b (- n 1) (* a b))))))


;; Exos 1.17
;; Design a log-time multiplying routine analagous to fast-expt

(define (double a) (* a 2))
(define (halve a) (/ a 2))

(define (fast-times a b) ; where (a*b) is invariant
  (cond ((= b 0) 0)
        ((= b 1) a)
        (else
         (if (even? b)
             (fast-times (double a) (halve b))  ;  a * (2b) = (2a) * b
             (+ a (fast-times a (- b 1)))       ; a * (2b + 1) = a + 2ab
             ))))

;; Exos 1.18
;; Design an iterative log-time multiplying routine analagoust to
;; my-expt in terms of halve, double, and adding.

(define (even? x) (= (remainder x 2) 0))

(define (iter-times a b m) ;; a*b + m is constant
  (cond ((= b 0) 0)
        ((= b 1) (+ a m))
        (else
         (if (even? b)
             (iter-times (double a) (halve b) m)
             (iter-times a (- b 1) (+ m a))))))

(define (my-times a b) (iter-times a b 0))
;; doesn't work for negatives; trap that in my-times and convert it
;; a*-b = - (a*b) to remedy.


;; Exos 1.19
;; By the power of algebra, I have determined that:
;;  p' = p^2 + q^2
;;  q' = q^2 + 2pq
;; so the completed routine is
(define (fib n)
  (fib-iter 1 0 0 1 n))

(define (fib-iter a b p q count)
  (cond ((= count 0) b)
        ((even? count)
         (fib-iter a
                   b
                   (+ (* p p) (* q q))
                   (+ (* q q) (* 2 p q))
                   (/ count 2)))
        (else (fib-iter (+ (* b q) (* a q) (* a p))
                        (+ (* b p) (* a q))
                        p
                        q
                        (- count 1)))))


;;Exos 1.20
;; TODO: don't get the point of this one, need to re-visit/look up an explanation.
(define (gcd a b)
  (if (= b 0)
      a
      (gcd b (remainder a b))))

;; With normal order,
;;(gcd 206 40) ;1
;;(gcd 40 6) ;2
;;(gcd 6 4) ;3
;;(gcd 4 2) ;4
;;(gcd 2 0) ;returns 2

;; With applicative order,
;;(gcd 206 40) ;1
;;(gcd 40 6) ;2
;;(gcd 6 4) ;3
;;(gcd 4 2) ;4
;;(gcd 2 0) ; returns 2


;; Exos 1.21
;; 
(define (divides? a b) (= (remainder b a ) 0))
(define (find-divisor n test-divisor)
  (cond ((> (square test-divisor) n) n)
        ((divides? test-divisor n) test-divisor)
        (else (find-divisor n (+ test-divisor 1)))))

(define (smallest-divisor n) (find-divisor n 2))

;;(smallest-divisor 199)                  ; 199
;;(smallest-divisor 1999)                 ; 1999
;;(smallest-divisor 19999)                ; 7

;; Exos 1.22
(define (prime? n) (= n (smallest-divisor n)))
(define (report-prime n elapsed-time)
  (newline)
  (display " *** ")
  (display `(,n ,elapsed-time)))
(define (start-prime-test n start-time)
  (if (prime? n)
      (report-prime n (- (runtime) start-time))))
(define (timed-prime-test n)
  (start-prime-test n (runtime)))


(define (find-primes-between start stop)
  (do ((i (if (= 0 (remainder start 2))
              (+ start 1)
              start) (+ i 2))) ((> i stop) stop)
    (timed-prime-test i)))
;; This excercise didn't really work; my computer just returned 0 for
;; all tests (except for some which had something like 0.199e-2).
;; Durned modern processors!

;; Exos 1.23
;; Skipping this one because it depends on 1.22

;;Exos 1.24
;; ditto

>;; Exos 1.25
(define (fast-expt b n)
  (cond ((= n 0) 1)
        ((even? n) (square (fast-expt b (/ n 2))))
        (else (* b (fast-expt b (- n 1))))))

(define (expmod base exp m)
  (cond ((= exp 0) 1)
        ((even? exp)
         (remainder
          (square (expmod base (/ exp 2) m))
          m))
        (else
         (remainder
          (* base (expmod base (- exp 1) m))
          m))))

;;Writing this with fast-exp would be rubish because you're computing
;;huge powers of the base. We're using log-laws and fermat's little
;;theorem to not do that.

;; Exos 1.26
;; Loui's given his recursive procedure a branching factor of 2,
;; which cancels out the log savings he'd made.  Poor Louis.


;; Exos 1.27
;; Charmichael numbers: 561 1105 1729 2465 2821 6601
;; goal: procedure that verifies (a^n
;; congruent means "a mod m = b mod m"

(define (congruence-check a n)
  (= (expmod a n n) (remainder a n)))

(define (carmichael-check a n)
  (cond ((= a n) true)
        ((congruence-check a n) (carmichael-check (+ a 1) n))
        (else false)))

(define (is-charmichael? n)
  (and (not (prime? n))
       (carmichael-check 1 n)))

;;(map is-charmichael? '(561 1105 1729 2465 2821 6601))
;; returns expected result


;; Exos 1.28 : Miller Rabin Test
;; * Random Number a in 1..n
;; * find (expmod a (- n 1) n)
;; BUT!
;;      * check for non-trivial sqrt of 1 when squaring
;;        i.e. m where m**2 mod m = 1 AND not= 1 or n-1

(define (make-mr-expmod n)
  (lambda (base exp m)
    (cond ((= exp 0) 1)
          ((even? exp)
           (let ((r (expmod base (/ exp 2) m)))
             (cond ((= r 0) 0) ;; report non-trivial root of 1 mod n
                   ((and (not (= 1 r))
                         (not (= (- n 1) r))
                         (= 1 (remainder (square r) n)))
                    0);; if we've found a nontrivial root of 1
                   (else (remainder (square r) m)))))
          (else
           (remainder
            (* base (expmod base (- exp 1) m))
            m)))))

(define (mr-test n)
  (define expmod (make-mr-expmod n))
  (define (check a)
    (let ((r (expmod a (- n 1) n)))
      (cond ((= r 1) #t)
            (else false))))
  (check (+ 1 (random (- n 1)))))

(define (mr-prime n times)
  (cond ((= times 0) true)
        ((mr-test n) (mr-prime n (- times 1)))
        (else #f)))
(define some-primes '( 5189   5197   5209   5227   5231   5233   5237   5261   5273   5279 
                              5281   5297   5303   5309   5323   5333   5347   5351   5381   5387 
                              5393   5399   5407   5413   5417   5419   5431   5437   5441   5443 
                              5449   5471   5477   5479   5483   5501   5503   5507   5519   5521 
                              5527   5531   5557   5563   5569   5573   5581   5591   5623   5639 
                              5641   5647   5651   5653   5657   5659   5669   5683   5689   5693 
                              5701   5711   5717   5737   5741   5743   5749   5779   5783   5791 
                              5801   5807   5813   5821   5827   5839   5843   5849   5851   5857 
                              5861   5867   5869   5879   5881   5897   5903   5923   5927   5939 
                              5953   5981   5987   6007   6011   6029   6037   6043   6047   6053 
                              6067   6073   6079   6089   6091   6101   6113   6121   6131   6133 
                              6143   6151   6163   6173   6197   6199   6203   6211   6217   6221 
                              6229   6247   6257   6263   6269   6271   6277   6287   6299   6301 
                              6311   6317   6323   6329   6337   6343   6353   6359   6361   6367 
                              6373   6379   6389   6397   6421   6427   6449   6451   6469   6473 
                              6481   6491   6521   6529   6547   6551   6553   6563   6569   6571 
                              6577   6581   6599   6607   6619   6637   6653   6659   6661   6673 
                              6679   6689   6691   6701   6703   6709   6719   6733   6737   6761 
                              6763   6779   6781   6791   6793   6803   6823   6827   6829   6833 
                              6841   6857   6863   6869   6871   6883   6899   6907   6911   6917 
                              6947   6949   6959   6961   6967   6971   6977   6983   6991   6997 
                              7001   7013   7019   7027   7039   7043   7057   7069   7079   7103 
                              7109   7121   7127   7129   7151   7159   7177   7187   7193   7207
                              7211   7213   7219   7229   7237   7243   7247   7253   7283   7297 
                              7307   7309   7321   7331   7333   7349   7351   7369   7393   7411 
                              7417   7433   7451   7457   7459   7477   7481   7487   7489   7499 
                              7507   7517   7523   7529   7537   7541   7547   7549   7559   7561 
                              7573   7577   7583   7589   7591   7603   7607   7621   7639   7643 
                              7649   7669   7673   7681   7687   7691   7699   7703   7717   7723 
                              7727   7741   7753   7757   7759   7789   7793   7817   7823   7829 
                              7841   7853   7867   7873   7877   7879   7883   7901   7907   7919 ))

;;(map (lambda (n) (mr-prime n 1000)) some-primes) ;; all true as expected
;;(map (lambda (n) (mr-prime n 1000)) (map (lambda (n) (+ n 1)) some-primes)) ;; all false as expected
;;(map (lambda (n) (mr-prime n 1000)) (map (lambda (n) (+ n (random 5))) some-primes)) ;; a mix, as expected
;; seems to work!


;; Exos 1.29





