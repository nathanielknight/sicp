;;;; Begin chapter 2 (rest of chapter 1 exos are on my other hard-drive)
;;(load "./chapter-01.scm")

;; Wishfully thinking that we have
;;   make-rat n d -> r
;;   numer r -> n
;;   denomm r -> d
;; we can define
(define (add-rat r1 r2)
  (make-rat (+ (* (numer r1) (denom r2))
               (* (numer r1) (denom r2)))
            (* (denom r1) (denom r2))))

(define (sub-rat r1 r2)
  (make-rat (- (* (numer r1) (denom r2))
               (* (numer r1) (denom r2)))
            (* (denom r1) (denom r2))))

(define (mul-rat r1 r2)
  (make-rat (* (numer r1) (numer r2))
            (* (denom r1) (denom r2))))

(define (div-rat r1 r2)
  (make-rat (* (numer r1) (denom r2))
            (* (numer r2) (denom r1))))

(define (equal-rat? r1 r2)
  (= (* (denom r1) (numer r2))
     (* (numer r1) (denom r2))))


;; Now that we know what a cons cell is, we can define
;;;; Exos 2.1 (improved make-rat)
(define (sign x)
  (if (< x 0) -1 1))
(define (make-rat n d)
  (let ((g (abs (gcd n d))))
    (cons (/ (* (sign n) (sign d) (abs n)) g)
          (/ (abs d) g))))
(define (numer r) (car r))
(define (denom r) (cdr r))

;; and for convenience,

(define (print-rat x)
  (newline)
  (display (numer x))
  (display "/")
  (display (denom x)))


;;;; Exos 2.2 Line Segments
(define (make-point x y)
  (cons x y))
(define (x-point p)
  (car p))
(define (y-point p)
  (cdr p))
(define (print-point p)
  (let ((x (x-point p))
        (y (y-point p)))
    (newline)
    (display "(")
    (display x)
    (display " ")
    (display y)
    (display ")")))

(define (make-segment a b)
  (cons a b))
(define (start-segment s)
  (car s))
(define (end-segment s)
  (cdr s))

(define (midpoint-segment s)
  (define  (avg x y)
    (/ (+ x y) 2))
  (let ((x1 (x-point (start-segment s)))
        (x2 (x-point (end-segment s)))
        (y1 (y-point (start-segment s)))
        (y2 (y-point (end-segment s))))
    (make-point
     (avg x1 x2)
     (avg y1 y2))))



;;;; Exos 2.3 Rectangles
;; Create constructor & selectors
;; With them, define perimiter and area

;;; Impelementation 1
;; constructor
(define (make-rect ll ur)
  "Create a rectangle with lower-left and upper-right corner"
  (cons ll ur))

;; selectors
(define (rect-ll r)
  (car r))
(define (rect-ur r)
  (cdr r))
(define (rect-lr r)
  (let ((x (x-point (rect-ur r)))
        (y (y-point (rect-ll r))))
    (make-point x y)))
(define (rect-ul r)
  (let ((x (x-point (rect-ll r)))
        (y (y-point (rect-ur r))))
    (make-point x y)))

(define (width r)
  (abs (- (x-point (rect-ul r))
          (x-point (rect-ur r)))))
(define (height r)
  (abs (- (y-point (rect-ul r))
                  (y-point (rect-ll r)))))

;;; Implementation 2
(define (make-rect ll wh)
  (cons ll wh))

(define (width r)
  (x-point (cdr r)))
(define (height r)
  (y-point (cdr r)))

;;; Derived properties (width and height are all you need, hooray!)
(define (rect-area r)
  (* (width r) (height r)))
(define (rect-perim r)
  (+ (* 2 (height r)) (* 2 (width r))))


;;;; Exos 2.4 Alt' Cons 1
;; Given this (very strange) definition of cons & car
;; (define (cons x y)
;;   (lambda (m) (m x y)))
;; (define (car z)
;;   (z (lambda (p q) p)))

;; the corresponding definition of cdr is
;; (define (cdr z)
;;   (z (lambda (p q) q)))


;;;; Exos 2.5
;; Define a pair of integers (a b) as 2**a * 3**b
;; Show that we can car and cdr it. Yay math.
;; (define (cons a b)
;;   (* (expt 2 a) (expt 3 b)))
;; (define (car p)
;;   (define (twos-iter x n)
;;  (if (not (even? x))
;;      n
;;      (twos-iter (/ x 2) (+ n 1))))
;;   (twos-iter p 0))
;; (define (cdr p)
;;   (define (threes-iter x n)
;;  (if (not (divides? 3 x))
;;      n
;;      (threes-iter (/ x 3) (+ n 1))))
;;   (threes-iter x 0))


;;;; Exos 2.6 Church numbers
;; I cheated and looked up the Church numbers on Wikipedia
;; https://en.wikipedia.org/wiki/Church_encoding
;; Turns out n = lambda (f) (lambda (x) ((f**n) x))
;; where f**n is the n-times compostion of f.
(define zero (lambda (f) (lambda (x) x)))
(define (add-1 n)
  (lambda (f)
    (lambda (x)
      (f
       ((n f) x)))))

;; Directly define one and two.
;; By applyting the substitution model, we find

;;(define one (add-1 zero))

;; (define one
;;   (lambda (f)
;;	(lambda (x)
;;    (f
;;     ((zero f) x)))))

;; (define one
;;   (lambda (f)
;;(lambda (x) (f ((lambda (x) (x)) x)))

(define one
  (lambda (f)
    (lambda (x) (f x))))

;; And similarly,
(define two
  (lambda (f)
    (lambda (x) (f (f x)))))


;; Define + without recourse to add-1
(define (church-add n m)
  (lambda (f) (lambda (x)
                (((m f) (n f)) x))))


;;;; Exos 2.7
;; With alyssa's implementation
(define (make-interval a b) (cons a b))
(define upper-bound car)
(define lower-bound cdr)

;;;;; Exos 2.8
;; With reasoning like alyssa's
;; [a,A] - [b,B] = [a - B, A - b]
(define (sub-interval x y)
  (make-interval
   (- (lower-bound x) (upper-bound y))
   (- (upper-bound x) (lower-bound y))))


;;;; Exos 2.9
;; Prove that the width of an interval sum is a function only of the widths of its summands.
;; The width of a summed interval X=[x1,x2] is
;;		x2 - x1
;; if
;;		X = A + B = [a1,a2] + [b1,b2] = [a1+b1,a2+b2]
;; then
;;		width(X) =
;;		x2 - x1 =
;;		a2 + b2 - a1 - b1 =
;;		a2 - a1 + b2 - a1 =
;;		width(A) - width(B)
;; QED

;; This is not the case for multiplication:
;; Eg:
;;		[0,1] * [0 1] = [0,1]  (width 1 * width 1 = width 1)
;;	vs. [0,1] * [99,100] = [0, 100] (width 1 * width 1 = width 100)
;;
;; Similarly for dividsion,
;; Eg:
;;		[0,1] / [0,1] = undefined
;;	vs. [1,2] / [1,2] = [1,2] * [0.5,1] = [0.5, 2]


;;;; Exos 2.10
;; Division with Error Checking
(define (div-interval x y)
  (if (or (= 0 (upper-bound y)) (= 0 (lower-bound y)))
      (error "div-interval called with a zero-bound in the denominator.")
      (mul-interval
       x
       (make-interval (/ 1.0 (upper-bound y))
                      (/ 1.0 (lower-bound y))))))



;;;; Exos 2.11
(define (orig-mul-interval x y)
  (let ((p1 (* (lower-bound x) (lower-bound y)))
        (p2 (* (lower-bound x) (upper-bound y)))
        (p3 (* (upper-bound x) (lower-bound y)))
        (p4 (* (upper-bound x) (upper-bound y))))
    (make-interval (min p1 p2 p3 p4)
                   (max p1 p2 p3 p4))))

;; mul-interval -> cases to reduce multiplication
;; [x1,x2] * [y1,y2] -> [z1,z2]
;; -------   -------    -------

;; TODO: Enumerate possibilites with +,-,0 in each position
;;       Eliminate impossible ones (like 0,0 & +,-) and dupes
;;       Treat each case


;;;; Exos 2.12 - Percentage Intervals
(define (make-center-percent c p)
  (let ((dc (/ (* p (abs c)) 100)))
    (make-interval (- c dc) (+ c dc))))
(define (center i)
  (/ (+ (lower-bound i) (upper-bound i)) 2))
(define (width i)
  (abs
   (- (upper-bound i) (lower-bound i))))
(define (percentage i)
  (let ((c (center i))
        (w (width i)))
    (* 100	(/ w c 2))))


;;;; Exos 2.13
;; For positive numbers with relatively small sentences, we can solve for the
;; percentage tolerance of the product of two intervals.
;; (x dx) * (y dy) = (z dz)
;; z = x y
;; pz	= z / ((x+dx)(y+dy) - (x-dx)(y-dy))
;;		= xy / (xy + x dy + y dx + dx dy - (xy - x dy - y dx + dx dy))
;;		= z / (2 x dy + 2 y dx)
;;		= 1 / (2 dy/y + 2 dx/x)
;;		= 1 / (2 px + 2 py)

;;;; Exos 2.14
;; Demonstrating that the implementation has inconsistencies.
;; Text's implementations
(define (add-interval x y)
  (make-interval (+ (lower-bound x) (lower-bound y))
                 (+ (upper-bound x) (upper-bound y))))
(define (mul-interval x y)
  (let ((p1 (* (lower-bound x) (lower-bound y)))
        (p2 (* (lower-bound x) (upper-bound y)))
        (p3 (* (upper-bound x) (lower-bound y)))
        (p4 (* (upper-bound x) (upper-bound y))))
    (make-interval (min p1 p2 p3 p4)
                   (max p1 p2 p3 p4))))
;; Lem's Functions
(define (par1 r1 r2)
  (div-interval (mul-interval r1 r2)
                (add-interval r1 r2)))

(define (par2 r1 r2)
  (let ((one (make-interval 1 1)))
    (div-interval
     one (add-interval (div-interval one r1)
                       (div-interval one r2)))))

;; Demonstrating Discrepancies
(define (display-center-percent i)
  (newline)
  (display (center i))
  (display " ")
  (display (percentage i)))


(let ((a (make-interval 2.1 2.2))
      (b (make-interval 3.5 3.7)))
  (display-center-percent (par1 a b))
  (display-center-percent (par2 a b))
  (display-center-percent (div-interval a b))
  (display-center-percent a)
  (display-center-percent (div-interval a a)))
;; Empirically it works


;;;; Exos 2.15
;; Is it better to have a program that does not repeat variables?
;; Empirically it is.

;; Since we take the worst case every time we performa a calculation
;; on an interval it makes sense that it would get worse the more
;; operations we do.


;;;; Exos 2.16
;; "Equivalent algebraic expressions" implies that the functions
;; defined above have all the properties of the algebraic functions
;; they're replacing. They don't: they're not associative.

(let ((a (make-interval 1.9 2.1))
      (b (make-interval 2.9 3.1))
      (c (make-interval 3.9 4.1)))
  (display-center-percent (add-interval
                           a
                           (add-interval b c)))
  (display-center-percent (add-interval
                           (add-interval a b)
                           c)))

;; We'd have to construct functions that were commutative and
;; associative if we wanted them to have the same properties as the
;; algebraic operations. I'm going to skip this one for now.


;;;; Moving on to pairs :)

;;;; Exos 2.17
(define (last-pair xs)
  (if (null? (cdr xs))
      (car xs)
      (last-pair (cdr xs))))

;;;; Exos 2.18
(define (reverse xs)
  (define (inner-reverse xs rs)
    (if (null? xs)
        rs
        (inner-reverse
         (cdr xs)
         (cons (car xs) rs))))
  (inner-reverse xs '()))

;;;; Exos 2.19 Revisiting Change Making
(define us-coins '(50 25 10 5 1))
(define uk-coins '(100 50 20 10 5 2 1 0.5))

(define first-denomination car)
(define except-first-denomination cdr)
(define no-more? null?)

(define (cc amount coin-values)
  (cond ((= amount 0) 1)
        ((or (< amount 0) (no-more? coin-values)) 0)
        (else
         (+ (cc amount (except-first-denomination coin-values))
            (cc (- amount (first-denomination coin-values))
                coin-values)))))

;; The order of the list of coins doesn't seem to affect the outcome,
;; I expect because `cc` will expand into a tree of nested +, 0 and 1
;; which should combine commutatively.

;;;; Exos 2.20
(define (same-parity x . xs)
  (cond ((= 0 x) '(x))
        ((even? x) (cons x (filter even? xs)))
        ((odd? x) (cons x (filter odd? xs)))))

;;;; Exos 2.21
;; Fill in the blanks:
;; (define (square-list items)
;;   (if (null? items)
;;    nil
;;    (cons
;;     ⟨ ?? ⟩ ⟨ ?? ⟩ )))
;; (define (square-list items)
;;   (map
;;    ⟨ ?? ⟩ ⟨ ?? ⟩ ))
(define nil '())
(define (square-list1 items)
  (if (null? items)
      nil
      (cons
       (* (car items) (car items)) (square-list1 (cdr items)))))
(define (square-list2 items)
  (map
   (lambda (x) (* x x))
   items))


;;;; Exos 2.22
;; Louis's first procedure will reverse the list because he's cons-ing
;; the car of his list onto nil to construct the results list, instead
;; of the last element (which would give the desired result).
;;
;; Now he's consing onto the result instead of nil, so he's constructing
;; a different style of list. (cons nil (cons 1 (cons 2 3))) instead of
;; (cons 1 (cons 2 (cons 3 nil)))

;;;; Exos 2.23
(define (nk-for-each f xs)
  (if (null? xs)
      true
      ((lambda (xs)
         (f (car xs))
         (nk-for-each f (cdr xs)))
       xs)))


;;; Exos 2.24
;; (list 1 (list 2 (list 3 4)))
;; -> (1 (2 (3 4)))

;; In terms of cells,
;; (see drawing)

;; As a tree:
;; -- - 1
;;   |-- - 2
;;      |-- - 3
;;        | - 4

;;;; Exos 2.25
;; pull 7 out of

;; a) (1 3 (5 7) 9)
(car (cdr (car (cdr (cdr '(1 3 (5 7) 9))))))

;; b) ((7))
(car (car '((7))))

;; c) (1 (2 (3 (4 (5 (6 7))))))
(cadr (cadr (cadr (cadr (cadr (cadr '(1 (2 (3 (4 (5 (6 7))))))))))))

;;;; Exos 2.26
(define x '(1 2 3))
(define y '(4 5 6))
;; (append x y) -> (1 2 3 4 5 6)
;; (cons x y) ((1 2 3) 4 5 6)
;; (list x y) ((1 2 3) (4 5 6))


;;;; Exos 2.27
(define (deep-reverse xs)
  (if (not (pair? xs))
      xs
      (reverse
       (map deep-reverse xs))))

;;;; Exos 2.28
(define (fringe xs)
  (cond ((null? xs) xs)
        ((not (pair? xs)) (list xs))
        (else (append (fringe (car xs))
                    (fringe (cdr xs))))))


;;;; Exos 2.29 : The Binary Mobile
(define (make-mobile left right)
  (list left right))
(define (make-branch length structure)
  (list length structure))

;; a) accessors
(define (left-branch mobile)
  (car mobile))
(define (right-branch mobile)
  (cadr mobile))

(define (branch-length branch)
  (car branch))
(define (branch-structure branch)
  (cadr branch))

(define (mobile? structure)
  (pair? structure))

;; b) total weight
(define (total-weight structure)
  (define (branch-weight b)
    (let ((s (branch-structure b)))
      (if (number? s)
          s
          (total-weight s))))
  (if (not (mobile? structure))
      structure
      (+ (branch-weight (left-branch structure))
         (branch-weight (right-branch structure)))))

;; c) balanced
(define (balanced? m)
  (define (torque branch)
    (* (branch-length branch) (total-weight (branch-structure branch))))
  (if (not (mobile? m))
      #t
      (and (= (torque (left-branch m)) (torque (right-branch m)))
           (balanced? (branch-structure (left-branch m)))
           (balanced? (branch-structure (right-branch m))))))

;; d) change?  If we change the constructors, my program is okay
;; (because it uses the constructors consistently and I got lucky and
;; used the right predicate to test whether things were mobiles or
;; not).



;;;; Exos 2.30
(define (square-tree-direct tree)
    (cond ((null? tree) '())
          ((not (pair? (car tree)))
           (cons (* (car tree) (car tree))
                 (square-tree-direct (cdr tree))))
          (else
           (cons (square-tree-direct (car tree))
                 (square-tree-direct (cdr tree))))))

(define (square-tree tree)
  (map (lambda (x)
         (if (pair? x)
             (square-tree x)
             (* x x)))
       tree))

;;;; Exos 2.31
(define (tree-map f t)
  (map (lambda (x)
         (if (pair? x)
             (tree-map f x)
             (f x)))
       t))


;;;; Exos 2.32
(define (subsets s)
  (if (null? s)
      (list '())
      (let ((rest (subsets (cdr s))))
        (append rest (map (lambda (x) (cons (car s) x)) rest)))))
;; The solution divides the subsets of x into two groups: those that
;; contain its first element and those that don't. It constructs the
;; second group recursively and then combines it with the subsets that
;; do contain the first element by adding it to each element in the
;; subset.  In the base case, it returns an empy list, onto which the
;; individual elements are consed when the set is exhausted.


;;;; Book definitions
(define (accumulate op initial sequence)
  (if (null? sequence)
      initial
      (op (car sequence)
          (accumulate op initial (cdr sequence)))))

(define (enumerate-interval low high)
  (if (> low high)
      nil
      (cons low (enumerate-interval (+ 1 low ) high))))

(define (enumerate-tree tree)
  (cond ((null? tree) nil)
        ((not (pair? tree)) (list tree))
        (else (append (enumerate-tree (car tree))
                      (enumerate-tree (cdr tree))))))

(define (sum-odd-squares tree)
  (accumulate + 0 (map square (filter odd? (enumerate-tree tree)))))

(define (even-fibs n)
  (accumulate
   cons
   nil
   (filter even?  (map fib  (enumerate-interval 0 n)))))


;;;; Exos 2.33 Fill-in the blanks

(define (map p sequence)
  (accumulate (lambda (x y) (cons (p x) y)) nil sequence))

(define (append seq1 seq2)
  (accumulate cons seq2 seq1))

(define (length seq)
  (accumulate
   (lambda (x l)
     (+ 1 l))
   0
   seq))


;;;; Exos 2.34 Evaluating Polynomials with Horner's Rule
(op (car cs)
    (accumulate op initial (cdr seq)))

(define (horner-eval x coeffs)
  (accumulate
   (lambda (c cs)
     (+ (* x cs)
        c))
   0
   coeffs))


;;;; Exos 2.35 Replacing count-leaves
(define (new-count-leaves t)
  (accumulate
   +
   0
   (map (lambda (x) 1) (enumerate-tree t))))

;;;; Exos 2.36
(define (accumulate-n op init seqs)
  (if (null? (car seqs))
      '()
      (cons (accumulate op init (map car seqs))
            (accumulate-n op init (map cdr seqs)))))

;;;; Exos 2.37 Extended Example: Vectors and Matrices
(define (dot-product v w)
  (accumulate + 0 (map * v w)))

(define (matrix-*-vector m v)
  (map (lambda (r) (dot-product r v)) m))

(define (transpose m)
  (accumulate-n cons '() m))

(define (matrix-*-matrix m n)
  (let ((cols (transpose n)))
    (map (lambda (r) (matrix-*-vector cols r)) m)))

;;;; Exos 2.38 Fold-left
(define (fold-left op init seq)
  (define (iter result rest)
    (if (null? rest)
        result
        (iter (op result (car rest))
              (cdr rest))))
  (iter init seq))

;; by inspection and then execution...
(fold-right / 1 (list 1 2 3)) ;; = (/ 3 2) t
(fold-left / 1 (list 1 2 3)) ;; = (/ 1 6) t
(fold-right list '() (list 1 2 3)) ;; = (1 2 3) f (1 (2 (3 ())))
(fold-left list '() (list 1 2 3)) ;; = (((() 1) 2) 3) t

;; if `op` is commutative, fold-left and fold-right should produce the same value.
;; for example:
(= (fold-left + 0 '(1 2 3 4))
   (fold-right + 0 '(1 2 3 4)))


;;;; Exos 2.39 New `reverse`
(define (reverse-fr seq)
  (fold-right (lambda (x xs) (append xs (list x))) '() seq))

(define (reverse-fl seq)
  (fold-left (lambda (xs x) (cons x xs)) '() seq))


;;;; (example from book)
(define (flatmap proc seq)
  (accumulate append nil (map proc seq)))

(define (prime-pair? ij)
  (prime? (+ (car ij) (cadr ij))))

(define (make-pair-sum ij)
  (list (car ij) (cadr ij) (+ (car ij) (cadr ij))))

(define (prime-pair-sums n)
  (map make-pair-sum
       (filter prime-pair?
               (flatmap (lambda (i)
                          (map (lambda (j) (list i j)) (enumerate-interval 1 (- i 1))))
                        (enumerate-interval 1 n)))))

;;;; Exos 2.40
(define (unique-pairs n)
  (flatmap (lambda (i)
             (map (lambda (j) (list i j))
                  (enumerate-interval 1 (- i 1))))
           (enumerate-interval 1 n)))

(define (simpler-prime-pair-sums n)
  (map make-pair-sum
       (filter prime-pair? (unique-pairs n))))


;;; Exos 2.41 (find ordered-triples i,j,k for k<j<i<=n such that i+j+k == s)

(define (unique-triples n)
  (flatmap (lambda (i)
             (flatmap (lambda (j) (map (lambda (k) (list k j i))
                                   (enumerate-interval 1 (- j 1))))
                  (enumerate-interval 1 (- i 1))))
           (enumerate-interval 1 n)))

(define (triples-that-sum-to s n)
  (define (sum-to-s? xs)
    (= s (apply + xs)))
  (filter sum-to-s? (unique-triples n)))


;;; Exos 2.42 Eight Queens (my first time solving this notorious puzzle!)

;;Board represented as a list of coordinates (x y) (not (row col)).
(define empty-board '())

(define (adjoin-position new-row k rest-of-queens)
  (cons (cons k new-row)
        (if (null? rest-of-queens)
            empty-board
            rest-of-queens)))

(define (safe? k positions)
  (define (diag-slope q1 q2)
    (if (= (car q1) (car q2))
        "inf"
        (/ (- (cdr q1) (cdr q2))
           (- (car q1) (car q2)))))
  (let ((nq (car positions))
        (oqs (cdr positions)))
    (cond
     ((null? oqs) #t) ;; only one queen
     ((any (lambda (oq) (= (car oq) (car nq))) oqs) #f)
     ((any (lambda (oq) (= (cdr oq) (cdr nq))) oqs) #f)
     ((any (lambda (oq) (or (= 1 (diag-slope oq nq))
                            (= -1 (diag-slope oq nq)))) oqs) #f)
     (#t #t))))

(define (queens board-size) ;; (from book)
  (define (queen-cols k)
    (if (= k 0)
        (list empty-board)
        (filter
         (lambda (positions) (safe? k positions))
         (flatmap
          (lambda (rest-of-queens)
            (map (lambda (new-row)
                   (adjoin-position
                    new-row k rest-of-queens))
                 (enumerate-interval 1 board-size)))
          (queen-cols (- k 1))))))
  (queen-cols board-size))

;;;; Exos 2.43
;; Louis is re-calculating the first k-1 rows for each new position candidate.
;; If the original program took T seconds, his should take T*T seconds.



;;;; Exos 2.44
;; Define the `up-split` procedure in  `corner-split` on page 178.
(define (up-split painter n)
  (if (= n 0)
      painter
      (let ((smaller (up-split painter (- n 1))))
        (below painter (beside smaller smaller)))))

;;;; Exos 2.45
;; Define `right-split` and `up-split` in terms of a more general `split`
;; which combines two combination operations
(define (split outer inner)
  (define (f painter n)
    (if (= n 0)
        painter
        (let ((smaller (f painter (- n 1))))
          (outer smaller (inner smaller smaller))))))

;; We demonstrate that this give the same result as the previous definitions
;; by expansion.

(define (right-split painter n)
  (if (= n 0)
      painter
      (let ((smaller (right-split painter (- n 1))))
        (beside painter (below smaller smaller)))))

(= right-split
   (split beside below)
   (define (f painter n)
     (if (= n 0)
         painter
         (let ((smaller (f painter (- n 1))))
           (beside  smaller (below smaller smaller)))))) ;; t

(= up-split
   (split below beside)
   (define (f painter n)
     (if (= n 0)
         painter
         (let ((smaller (f painter (- n 1))))
           (below smaller (beside smaller smaller)))))) ;; t

;;;; Exos 2.46
;; implement vector math using pairs
(define (make-vect xcorr ycorr)
  (cons xcorr ycorr))
(define (xcorr-vect vect)
  (car vect))
(define (ycorr-vect vect)
  (cdr vect))

(define (add-vect v1 v2)
  (make-vect
   (+ (xcorr-vect v1) (xcorr-vect v2))
   (+ (ycorr-vect v1) (ycorr-vect v2))))
(define (sub-vect v1 v2)
  (make-vect
   (- (xcorr-vect v1) (xcorr-vect v2))
   (- (ycorr-vect v1) (ycorr-vect v2))))
(define (scale-vect s v)
  (make-vect (* s (xcorr-vect v))
             (* s (ycorr-vect v))))


;;;; Exos 2.47
;; Supply selectors for the following data constructors:
;; Version 1
(define (make-frame origin edge1 edge2)
  (list origin edge1 edge2))
(define (origin-frame frame)
  (car frame))
(define (edge1-frame frame)
  (cadr frame))
(define (edge2-frame frame)
  (caddr 3))
;; Version 2
(define (make-frame origin edge1 edge2)
  (cons origin (cons edge1 edge2)))
(define origin-frame car)
(define edge1-frame cadr)
(define edge2-frame cddr)


;;;; Exos 2.48
;; Using the vectors from exos 2.46, define a line segment represented
;; as a vector from the origin to the start point and a vector from the
;; origin to the end-point
(define (make-segment start end)
  (cons start end))
(define start-segment car)
(define end-segment cdr)


;;;; Exos 2.49
;; Define a bunch of painters using `segments->painter`
(define (frame-coord-map frame)
  (lambda (v)
    (add-vect
     (origin-frame frame)
     (add-vect (scale-vect (xcor-vect v) (edge1-frame frame))
               (scale-vect (ycor-vect v) (edge2-frame frame))))))

(define (segments->painter segment-list)
  (lambda (frame)
    (for-each
     (lambda (segment)
       (draw-line
        ((frame-coord-map frame)
         (start-segment segment))
        ((frame-coord-map frame)
         (end-segment segment))))
     segment-list)))

;; a) Outline of designated frame
(define (outliner)
  (segments->painter
   (list
    (make-segment
     (make-vect 0 0)
     (make-vect 0 1))
    (make-segment
     (make-vect 0 0)
     (make-vect 1 0))
    (make-segment
     (make-vect 1 0)
     (make-vect 1 1))
    (make-segment
     (make-vect 0 1)
     (make-vect 1 1)))))

;; b) x through the frame
(define (diamond)
  (segments->painter
   (list
    (make-segment
     (make-vect 0 0 )
     (make-vect 1 1))
    (make-segment
     (make-vect 0 1)
     (make-vect 1 0)))))



;; c) diamdond by connecting opposite midpoints
(define (diamond)
  (segments->painter
   (list
    (make-segment
     (make-vect 0.5 0)
     (make-vect 0 0.5))
    (make-segment
     (make-vect 0.5 0)
     (make-vect 1 0.5))
    (make-segment
     (make-vect 0 0.5)
     (make-vect 0.5 1))
    (make-segment
     (make-vect 1 0.5)
     (make-vect 0.5 1)))))

;; d) the "wave" painter
;; no


;;;; Exos 2.50
;; Define the following transformations using
;; (transform-painter painter new-origin new-br new-tl)

;; a) Horizontal flip
(define (horizontal-flip painter)
  (let ((paint-flipped-horiz
         (transform-painter
          painter
          (make-vect 1 0)
          (make-vect 0 0)
          (make-vect 1 1))))
    (lambda (frame (paint-flipped-horiz frame)))))

;; b) counterclockwise 180-degrees
(define (rotate-anticlockwise-90 painter)
  (let ((paint-90
         (transform-painter
          painter
          (make-vect 1 0)
          (make-vect 1 1)
          (make-vect 0 0))))
    (lambda (frame)
      (rotate-anticlockwise-90 painter))))

(define (rotate-anticlockwise-180 painter)
  (rotate-90 (rotate-90 painter)))

;; c) counterclockwise 270-degrees
(define (rotate-anticlockwise-270 painter)
  (rotate-90 (rotate-180 painter)))


;;;; Exos 2.51
;; Implement `below`

;; a) as `beside` in the book
(define (below painter1 painter2)
  (let ((paint-top
         (transform-painter
          painter1
          (make-vect 0 0.5)
          (make-vect 1 0.5)
          (make-vect 1 0)))
        (paint-bottom
         (transform-painter
          painter2
          (make-vect 0 0)
          (make-vect 1 0)
          (make-vect 0 0.5))))
    (lambda (frame)
      (paint-top frame)
      (paint-bottom frame))))

;; b) In terms of rotations and `beside` (might not have defined all the
;;    rotations, but they're similar to the ones I defined above).
(define (below-rot painter1 painter2)
  (rotate-anticlockwise-90
   (beside (rotate-clockwise-90 painter2)
           (rotate-clockwise-90 painter1))))

;;;; Exos 2.52
;; Skipped.
;; Calls for a bunch of modifications to the "wave" painter, which I didn't
;; implement because it's picky and arbitrary, and I don't have a concrete
;; implementation of it to check my work.


;;;; Exos 2.53
;; What would be printed in response to:

(list 'a 'b 'c) ;; (a b c)
(list (list 'george)) ;; ((george))
(cdr '((x1 x2) (y1 y2))) ;; ((y1 y2))
(cadr '((x1 x2) (y1 y2))) ;; (y1 y2)
;; (I said x2, forgetting that cadr is car of cdr, not cdr of car
(pair? (car '(a short list))) ;; f (it's just 'a)
(memq 'red '((red shoes) (blue socks))) ;; f
(memq 'red '(red shoes blue socks)) ;; '(red shoes blue socks)

;;;; Exos 2.54
;; Define "equals?" recursively for lists
(define (my-equals? a b)
  (define (same-type? a b)
    (or (and (symbol? a) (symbol? b))
        (and (list? a) (list? b))
        (and (number? a) (number? b))
        (and (null? a) (null? b))))
  (cond
   ((not (same-type? a b)) false)
   ((null? a) true)
   ((number? a) (= a b))
   ((not (list? a)) (equal? a b))
   (t ;; two lists
    (and (my-equals? (car a) (car b))
         (my-equals? (cdr a) (cdr b))))))

;;;; Exos 2.55
;; Why does (car ''abracadabra) print 'quote'?
;; The reader interprets 'abracadabra as (quote abracadabra), and
;; ''abracadabra as (quote (quote abracadabra)). THe first quote suppresses
;; evaluation, the second gets picked out by car.


;;;; Symbolic differentiation (copied from the text)
(define (deriv exp var)
  (cond ((number? exp) 0)
        ((variable? exp) (if (same-variable? exp var) 1 0))
        ((sum? exp) (make-sum (deriv (addend exp) var)
                              (deriv (augend exp) var)))
        ((product? exp)
         (make-sum
          (make-product (multiplier exp)
                        (deriv (multiplicand exp) var))
          (make-product (deriv (multiplier exp) var)
                        (multiplicand exp))))))

(define variable? symbol?)
(define (same-variable? v1 v2)
  (and
   (variable? v1)
   (variable? v2)
   (eq? v1 v2)))
(define (=number? exp n)
  (and (number? exp) (= exp n)))

(define (make-sum addend augend)
  (cond ((=number? addend 0) augend)
        ((=number? augend 0) addend)
        ((and (number? addend) (number? augend))
         (+ addend augend))
        (else (list '+ addend augend))))
(define (sum? x)
  (and (pair? x)
       (eq? (car x) '+)))
(define addend cadr)
(define augend caddr)

(define (make-product multiplier multiplicand)
  (cond ((or (=number? multiplier 0) (=number? multiplicand 0))
         0)
        ((=number? multiplier 1) multiplicand)
        ((=number? multiplicand 1) multiplier)
        ((and (number? multiplier) (number? multiplicand))
         (* multiplier multiplicand))
        (else (list '* multiplier multiplicand))))

(define (product? x)
  (and (pair? x)
       (eq? (car x) '*)))
(define multiplier cadr)
(define multiplicand caddr)




;;;; Exos 2.56
;; Implementing the polynomical differentiation rule

;; Exponentiation
(define (make-exponentiation base exponent)
  (cond
   ((=number? base 0) 0)
   ((=number? exponent 1) base)
   ((=number? exponent 0) 1)
   ((and (number? base) (number? exponent)) (expt base exponent))
   (else (list '** base exponent))))

(define (exponent? expr)
  (and (pair? expr)
       (eq? (car expr) '**)))

(define base cadr)
(define exponent caddr)

;; Monomial differentiation rule

(define (monomial? exp var)
  (and (exponent? exp)
       (same-variable? (base exp) var)
       (not (same-variable? (exponent exp) var))))

(define (deriv-monomial exp var)
  (cond
   ((not (monomial? exp var)) (error "`[deriv-monomial] passed non-monomial expression " exp))
   ((and (monomial? exp var) (same-variable? var (base exp)))
    (make-product
     (exponent exp)
     (make-exponentiation (base exp)
                           (make-sum (exponent exp) -1))))
   (else (error "Don't know how to differentiate " exp))))


(define (deriv exp var)
  (cond ((number? exp) 0)
        ((variable? exp) (if (same-variable? exp var) 1 0))
        ((sum? exp) (make-sum (deriv (addend exp) var)
                              (deriv (augend exp) var)))
        ((product? exp)
         (make-sum
          (make-product (multiplier exp)
                        (deriv (multiplicand exp) var))
          (make-product (deriv (multiplier exp) var)
                        (multiplicand exp))))
        ((monomial? exp var)
         (deriv-monomial exp var))))

;;;; Exos 2.57
;; Add support for arbirary numbers of summands and multiplicands by only modifying
;; the definitions of sums and products
(define (make-sum addend . augend)
  (if (= 1 (length augend))
      (let ((augend (car augend)))
        (cond
         ((=number? addend 0) augend)
         ((=number? augend 0) addend)
         ((and (number? addend) (number? augend))
          (+ addend augend))
         (else (list '+ addend augend))))
      (list '+ addend
            (apply make-sum (car augend)
                   (cdr augend)))))

(define (addend exp)
  (cadr exp))

(define (augend exp)
  (if (= 3 (length exp))
      (caddr exp)
      (apply make-sum (cddr exp))))

(define (make-product multiplier . multiplicand)
  (if (= 1 (length multiplicand))
      (let ((multiplicand (car multiplicand)))
        (cond
         ((or (=number? multiplier 0) (=number? multiplicand 0))
          0)
         ((=number? multiplier 1) multiplicand)
         ((=number? multiplicand 1) multiplier)
         ((and (number? multiplicand) (number? multiplier))
          (* multiplier multiplicand))
         (else (list '* multiplier multiplicand))))
      (list '* multiplier
            (apply make-product (car multiplicand)
                   (cdr multiplicand)))))

(define (multiplier exp)
  (cadr exp))
(define (multiplicand exp)
  (if (= 3 (length exp))
      (caddr exp)
      (apply (make-product cddr exp))))


;;;; Exos 2.58 Skipped; we'll save it for macros and I'm kind of done with this example.

;;;; Exos 2.59 Unordered List representation of sets
;; from the book:
(define (element-of-set? x set)
  (cond ((null? set) false)
        ((equal? x (car set)) true)
        (else (element-of-set? x (cdr set)))))

(define (adjoin-set x set)
  (if (element-of-set? x set)
      set
      (cons x set)))

(define (intersection-set set1 set2)
  (cond ((or (null? set1) (null? set2)) '())
        ((element-of-set? (car set1) set2)
         (cons (car set1) (intersection-set (cdr set1) set2)))
        (else (intersection-set (cdr set1) set2))))

(define (union-set set1 set2)
  (cond
   ((null? set1) set2)
   ((null? set2) set1)
   ((element-of-set? (car set1) set2)
    (union-set (cdr set1) set2))
   (else (cons (car set1) (union-set (cdr set1) set2)))))


;;;; Exos 2.60 Non-unique unordered list representation of sets
(define (element-of-set? x set)
  (cond
   ((equal? set '()) false)
   ((equal? x (car set)) true)
   (else (element-of-set? x (cdr set)))))

(define (adjoin-set x set)
  (cons x set))

(define (intersection-set set1 set2)
  (cond ((or (null? set1) (null? set2)) '())
        ((element-of-set? (car set1) set2)
         (cons (car set1) (intersection-set (cdr set1) set2)))
        (else (intersection-set (cdr set1) set2))))

(define (union-set set1 set2)
  (if (null? set1)
      set2
      (union-set (cdr set1) (cons (car set1)  set2))))

;; This one has faster writes and unions but potentially very slow intersections.
;; The first is better when we want to compare sets quickly. This one's better
;; when we want to gather a bunch of information quickly.


;;;; Exos 2.61 Ordered list implementation of sets

;; from the book:
(define (element-of-set? x set)
  (cond ((null? set) false)
        ((= x (car set)) true)
        ((< x (car set)) false)
        (else (element-of-set? x (cdr set)))))
(define (intersection-set set1 set2)
  (if (or (null? set1) (null? set2))
      '()
      (let ((x1 (car set1)) (x2 (car set2)))
        (cond ((= x1 x2)
               (cons x1 (intersection-set (cdr set1)
                                          (cdr set2))))
              ((< x1 x2)
               (intersection-set (cdr set1) set2))
              ((< x2 x1)
               (intersection-set set1 (cdr set2)))))))

;; from me:
(define (adjoin-set x set)
  (cond
   ((null? set) (list x))
   ((< x (car set)) (cons x set))
   (else (cons (car set)
               (adjoin-set x (cdr set))))))

;; This version doesn't use `element-of-set`, which is O(n), and it only expects
;; to have to look at half the elements of the list (n/2 elements) before it finds
;; x's place. QED


;;;; Exos 2.62 O(n) implementation of `union-set`
(define (union-set xs ys)
  (define (inner-union xs ys result)
    (cond
     ;; termination
     ((and (null? xs) (null? ys))
      result)
     ((null? xs)
      (inner-union xs (cdr ys) (cons (car ys) result)))
     ((null? ys)
      (inner-union (cdr xs) ys (cons (car xs) result)))
     ;; reduction
     ((equal? (car xs) (car ys))
      (inner-union (cdr xs) (cdr ys) (cons (car xs) result)))
     ((< (car xs) (car ys))
      (inner-union (cdr xs) ys (cons (car xs) result)))
     ((> (car xs) (car ys))
      (inner-union xs (cdr ys) (cons (car ys) result)))))
  (reverse (inner-union xs ys '())))

;; The above implementation is linear: at every step of inner-union
;; the size of the input decreases by at least one, the reversal at
;; the end is linear in the size of its output, and the result of two
;; linear operations is linear.


;;;; Tree representation (from the book).

(define (entry tree) (car tree))
(define (left-branch tree ) (cadr tree))
(define (right-branch tree) (caddr tree))
(define (make-tree entry left right)
  (list entry left right))

(define (element-of-set? x set)
  (cond
   ((null? set) false)
   ((= x (entry set)) true)
   ((< x (entry set))
    (element-of-set? x (left-branch set)))
   ((> x (entry set))
    (element-of-set? x (right-branch set)))))

(define (adjoin-set x set)
  (cond
   ((null? set) (make-tree x '() '()))
   ((eq? x (entry set)) set)
   ((< x (entry set))
    (make-tree (entry set)
               (adjoin-set x (left-branch set))
               (right-branch set)))
   ((> x (entry set))
    (make-tree (entry set)
               (left-branch set)
               (adjoin-set x (right-branch set))))))

;;;; Exos 2.63

;; a)

;; They don't produce the same results. The first ends up with the
;; left tree, then the entry, then the right. The second does the
;; entry, then the right tree, then the left.

;; b)

;; The second is O(n) (a cons on each element).

;; The first has an append (linear) at each step, and each step cuts
;; the list in half, so it ends up as O(n log(n)).



;;;; Exos 2.64

;; From the book:
(define (list->tree elements)
  (car (partial-tree elements (length elements))))

(define (partial-tree elts n)
  (if (= n 0)
      (cons '() elts)
      (let ((left-size (quotient (- n 1) 2)))
        (let ((left-result (partial-tree elts left-size)))
          (let ((left-tree (car left-result))
                (non-left-elts (cdr left-result))
                (right-size (- n (+ left-size 1))))
            (let ((this-entry (car non-left-elts))
                  (right-result (partial-tree (cdr non-left-elts) right-size)))
              (let ((right-tree (car right-result))
                    (remaining-elts (cdr right-result)))
                (cons (make-tree this-entry
                                 left-tree
                                 right-tree)
                      remaining-elts))))))))

;; a) Partial helper takes a list of at most n elements and an integer
;; n.  It returns a pair with a tree containing n elements and a list
;; of elements not included in the list. How does it work?

;; When taking 0 elements, return the input list an an empty tree.
;; Otherwise, it splits the list into a left side, "this entry", and a
;; right side, and constructs a tree from the left and right sides,
;; making them the left and right branches of the tree about "this entry".
;; The left and right sides are reduced until they get to one element,
;; which becomes a leaf.

;; The tree formed by the list '(1 3 5 7 9 11) will be:
(define tree-result-64a
  '(5
    (1 ()  (3 () ()))
    (9
     (7 () ())
     (11 () ()))))


;; b) Order of growth:

;; For n = 0 we do a cons (O 1). For n elements we do a a cons, plus a
;; make tree on two lists of size ~ n/2, which demands work (* 2 (work
;; for for n/2)).  We split the sample and do 2x work at each step,
;; which ends up being linear.


;;;; Exos 12.65
;; A linear implementation of `union-set` and `intersection-set` on
;; balanced binary trees.

;; from the book
(define (tree->list-2 tree)
  (define (copy-to-list tree result-list)
    (if (null? tree)
        result-list
        (copy-to-list (left-branch tree)
                      (cons (entry tree)
                            (copy-to-list
                             (right-branch tree)
                             result-list)))))
  (copy-to-list tree '()))

;; Helpers
(define (merge-lists l1 l2)
  (cond
   ((null? l1) l2)
   ((null? l2) l1)
   (else (let ((x (car l1))
               (y (car l2)))
           (cond
            ((= x y) (cons x (merge-lists (cdr l1) (cdr l2))))
            ((< x y) (cons x (merge-lists (cdr l1) l2)))
            ((> x y) (cons y (merge-lists l1 (cdr l2)))))))))

(define (intersect-lists l1 l2)
  (cond
   ((null? l1) '())
   ((null? l2) '())
   (else (let ((x (car l1))
               (y (car l2)))
           (cond
            ((= x y) (cons x (intersect-lists (cdr l1) (cdr l2))))
            ((< x y) (intersect-lists (cdr l1) l2))
            ((> x y) (intersect-lists l1 (cdr l2))))))))

;; Solution
(define (linear-union-set set1 set2)
  (let ((list1 (sort (tree->list-2 set1) <)) ;; this step is linear
        (list2 (sort (tree->list-2 set2) <)))
    (list->tree (merge-lists list1 list2)))) ;; this step is linear


(define (linear-intersect-set set1 set2)
  (let ((list1 (sort (tree->list-2 set1))) ;; this step is linear
        (list2 (sort (tree->list-2 set2))))
    (list->tree (intersect-lists list1 list2)))) ;; this step is linear

;; Since a sequence of linear steps is linear, both of those are linear.


;;;; Exos 2.66 Implementing lookup for a binary tree (arranged by value)
;; Assuming dict is implemented as a binary tree of pairs of (key . value)
(define (tree-lookup x set-tree)
  (define (key x) (car x))
  (define (val x) (cdr x))
  (cond
   ((null? set-tree) false)
   ((eq? x (key (entry set-tree))) (val (entry set-tree)))
   ((< x (key (entry set-tree))) (tree-lookup x (left-branch set-tree)))
   (else (tree-lookup x (right-branch set-tree)))))



;; Huffman tree representation from the book
;; - trees
(define (make-leaf symbol weight) (list 'leaf symbol weight))
(define (leaf? obj) (eq? 'leaf (car obj)))
(define (symbol-leaf x) (cadr x))
(define (weight-leaf x) (caddr x))
;; - huffman trees
(define (symbols tree)
  (if (leaf? tree)
      (list (symbol-leaf tree))
      (caddr tree)))
(define (weight tree)
  (if (leaf? tree)
      (weight-leaf tree)
      (cadddr tree)))
(define (left-branch tree) (car tree))
(define (right-branch tree) (cadr tree))
(define (make-code-tree left right)
  (list left
        right
        (append (symbols left) (symbols right))
        (+ (weight left) (weight right))))


(define (choose-branch bit branch)
  (cond
   ((= bit 0) (left-branch branch))
   ((= bit 1) (right-branch branch))
   (else (error "bad bit: CHOOSE-BRANCH " bit))))
(define (decode bits tree)
  (define (decode-1 bits current-branch)
    (if (null? bits)
        '()
        (let ((next-branch
               (choose-branch (car bits) current-branch)))
          (if (leaf? next-branch)
              (cons (symbol-leaf next-branch)
                    (decode-1 (cdr bits) tree))
              (decode-1 (cdr bits) next-branch)))))
  (decode-1 bits tree))



;;;; Exos 2.67 Decoding a huffman encoded message
(define sample-tree
  (make-code-tree
   (make-leaf 'A 4)
   (make-code-tree
    (make-leaf 'B 2)
    (make-code-tree
     (make-leaf 'D 1)
     (make-leaf 'C 1)))))
(define sample-message '(0 1 1 0 0 1 0 1 0 1 1 1 0))

(decode sample-message sample-tree) ;; adabbca
;; checks out according to http://community.schemewiki.org/?sicp-ex-2.67


;;;; Exos 2.68 Writing `encode-symbol`

(define (encode message tree) ;; from the book
  (if (null? message)
      '()
      (append (encode-symbol (car message) tree)
              (encode (cdr message) tree))))

(define (has-symbol? s branch)
  (if (leaf? branch)
      (eq? s (symbol-leaf branch))
      (element-of-set? s (symbols branch))))

(define (encode-symbol s tree)
  (define (partial-encode-symbol s tree encoding)
    (if (has-symbol? s (left-branch tree))
        (if (leaf? (left-branch tree))
            (append encoding '(0))
            (partial-encode-symbol s (left-branch tree) (append encoding '(0))))
        (if (leaf? (right-branch tree))
            (append encoding '(1))
            (partial-encode-symbol s (right-branch tree) (append encoding '(1)))
            )))
  (if (not (element-of-set? s (symbols tree)))
      (error "Symbol not in tree: " s)
      (partial-encode-symbol s tree '())))
(encode '(a d a b b c a) sample-tree)  ;; (0 0 1 1 0 0 1 0 1 1 1 1 0)


;;;; Exos 2.69
(define (make-leaf-set pairs)
  (if (null? pairs)
      '()
      (let ((pair (car pairs)))
        (adjoin-set (make-leaf (car pair)
                               (cadr pair))
                    (make-leaf-set (cdr pairs))))))

(define (weight-sort weighted-items)
  (sort weighted-items (lambda (a b) (< (weight a) (weight b)))))

(define (generate-huffman-tree pairs)
  (define (successive-merge symbol-set)
    (if (< (length symbol-set) 2)
        (car symbol-set)
        (let ((sorted-elements (weight-sort symbol-set)))
          (successive-merge
           (cons
            (make-code-tree (cadr sorted-elements) (car sorted-elements))
            (cddr sorted-elements))))))
  (successive-merge (make-leaf-set pairs)))

;; testing:
(define pairs '((a 3) (b 1) (c 5) (d 2)))
(generate-huffman-tree pairs)


(define t-tree (generate-huffman-tree '((e 5) (a 4) (f 3) (g 2) (c 1))))
(define message '(a c e f a c e a g e f a g e))
(encode message t-tree)


;;;; Exos 2.70 Encoding 70s rock songs
(define rock-song '(Get a job
                   Sha na na na na na na na na
                   Get a job
                   Sha na na na na na na na na
                   Wah yip yip yip yip yip yip yip yip yip
                   Sha boom))
(define rock-alphabet '((a 2)
                   (boom 1)
                   (get 2)
                   (job 2)
                   (sha 3)
                   (na 16)
                   (wah 1)
                   (yip 9)))
(define rock-tree (generate-huffman-tree rock-alphabet))
(encode rock-song rock-tree) ;; (0 0 0 0 1 0 0 1 0 0 0 0 0 0 0 0 0 1 1 1 1 1 1 1 1 1 0 0 0 0 1 0 0 1 0 0 0 0 0 0 0 0 0 1 1 1 1 1 1 1 1 1 0 0 1 1 0 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 0 0 1 0 0 1 1 1)
(length (encode rock-song rock-tree)) ;; => 84
(decode (encode rock-song rock-tree) rock-tree) ;; => v rock-song

;; It requires 84 bits to encode the song (and it decodes!). The rock
;; song contains 36 symbols. A fixed length code would require 3 bits
;; to encode the 8-symbol rock alphabet. As such, a fixed length code
;; would require (* 8 36) => 288 bits to encode. We achieved a factor
;; of 3 compression :)


;;;; Exos 2.71 Trees with Power-of-Two weights

(define five-tree (generate-huffman-tree '((a 1)
                                           (b 2)
                                           (c 4)
                                           (d 8)
                                           (e 16)))) ;; =>

((leaf e 16)
 ((leaf d 8)
  ((leaf c 4)
   ((leaf b 2)
    (leaf a 1)
    (b a) 3)
   (c b a) 7)
  (d c b a) 15)
 (e d c b a) 31)

;; The most frequently used symbol requires 1 bit to encode. The least
;; frequently used symbol requires N bits, where N is the size of the
;; alphabet (because the m-least weighty elements will always have a total
;; weight that's less than the m+1'st element)

;; let exp(n) = 2**n
;; exp(n) = 2 * exp(n-1)
;;  vs.
;; let sum(n) = sum i=1..n 2**n
;; sum(n) = 1 + 2 * sum(n-1)
;;
;; Prove: sum(n-1) < exp(n) (by induction)
;; * at n=1, sum(0) = 0, exp(1) = 1
;; * if (exp(n) > sum(n-1)
;;   exp(n+1) = 2 * exp(n)
;;   sum(n) = 1 + 2*sum(n-1)
;;   2 * exp(n) > 1 + 2 * sum(n-1)
;;   exp(n) > 0.5 + sum(n-1)
;;   Because we're dealing in integers this holds for all n.
;; QED


;;;; Exos 2.72 Order of growth for encoding a symbol
;; Assume an power-of-two weighted alphabet of n elements
;; `encode-symbol` does the following:
;; - check if symbol is in the tree                                         O(n)
;; - Recursively:
;;   - Check the left branch for symbol                                     O(1)
;;   -  return '(1) with probablilty ~0.5                                   O(1)
;;   -  Go to the right brach with probability 0.5 and append '(0)          O(n-1)
;; - Reverse the result (my implementation doesn't do this, but it could)   O(n)

;; So we expect the average to be O(n) + sum((n-m)..n) where m is a
;; geometrically distributed random variable with p==0.5.
;; E(n-m) = n - E(m) = n -

;; sum i=1..inf | i * q**i-1 * p
;; p * sum i=1..inf i q**i-1
;; Anti-Differentiating in the sum,
;; p * derivative sum i=1..inf q**i
;; p * derivative (1 / (1-q))
;; p * (1-q)**2

;; For p = 0.5, E(m) = 2

;; So we expect the growth to be O(4n) = O(n) ?
;; TODO(nknight) check answer

;; For the most commonly used symbol, it will be straight up N steps
;; (just checking for presence). For the least commonly used it will
;; be n**2 (n steps of size O(n)).



;;;; Section 2.4 Multiple Representations of Abstract Data

;; Generic complex arithmetic (from the book)
(define (add-complex z1 z2)
  (make-from-real-imag (+ (real-part z1) (real-part z2))
                       (+ (imag-part z1) (imag-part z2))))
(define (sub-complex z1 z2)
  (make-from-real-imag (- (real-part z1) (real-part z2))
                       (- (imag-part z1) (imag-part z2))))
(define (mul-complex z1 z2)
  (make-from-mag-ang (* (magnitude z1) (magnitude z2))
                     (+ (angle z1) (angle z2))))
(define (div-complex z1 z2)
  (make-from-mag-ang (/ (magnitude z1) (magnitude z2))
                     (- (angle z1) (angle z2))))
;; need real-part, imag-part, make-from-real-imag, make-from-mag-ang

(define (attach-type-tag type-tag contents)
  (cons type-tag contents))
(define (type-tag obj)
  (if (pair? obj)
      (car obj)
      (error "(type-tag) Bad tagged type: " obj)))
(define (contents obj)
  (if (pair? obj)
      (cdr obj)
      (error "(contents) Bad tagged type:" obj)))

(define (rectangular? z)
  (eq? (type-tag z) 'rectangular))
(define (polar? z)
  (eq? (type-tag z) 'polar))

;;;; Ben & Alyssas implementations (rect & polar, respectively)

(define (real-part-rectangular z) (car z))
(define (imag-part-rectangular z) (cdr z))
(define (magnitude-rectangular z)
  (sqrt (+ (square (real-part-rectangular z))
           (square (imag-part-rectangular z)))))

(define (angle-rectangular z)
  (atan (imag-part-rectangular z)
        (real-part-rectangular z)))
(define (make-from-real-imag-rectangular x y)
  (attach-tag 'rectangular (cons x y)))
(define (make-from-mag-ang-rectangular r a)
  (attach-tag 'rectangular
              (cons (* r (cos a)) (* r (sin a)))))

(define (real-part-polar z)
  (* (magnitude-polar z) (cos (angle-polar z))))
(define (imag-part-polar z)
  (* (magnitude-polar z) (sin (angle-polar z))))
(define (magnitude-polar z) (car z))
(define (angle-polar z) (cdr z))
(define (make-from-real-imag-polar x y)
  (attach-tag 'polar
              (cons (sqrt (+ (square x) (square y)))
                    (atan y x))))
(define (make-from-mag-ang-polar r a)
  (attach-tag 'polar (cons r a)))

;;;; Generic code that uses either implementation:

(define (real-part z)
  (cond ((rectangular? z)
         (real-part-rectangular (contents z)))
        ((polar? z)
         (real-part-polar (contents z)))
        (else (error "Unknown type: REAL-PART" z))))
(define (imag-part z)
  (cond ((rectangular? z)
         (imag-part-rectangular (contents z)))
        ((polar? z)
         (imag-part-polar (contents z)))
        (else (error "Unknown type: IMAG-PART" z))))
(define (magnitude z)
  (cond ((rectangular? z)
         (magnitude-rectangular (contents z)))
        ((polar? z)
         (magnitude-polar (contents z)))
        (else (error "Unknown type: MAGNITUDE" z))))
(define (angle z)
  (cond ((rectangular? z)
         (angle-rectangular (contents z)))
        ((polar? z)
         (angle-polar (contents z)))
        (else (error "Unknown type: ANGLE" z))))


;;;; Exos 2.73
;; a) The program was rewritten to dispatch differentiation based on
;; the on operation in an expression rather than in a cond statement.
;;
;; number? and variable? couldn't be refactored in this way because
;; they don't have a "type" (or an operation) to dispatch on.

;; b) procedures for derivatives of sums and products and the
;; auxilliary code required to install them in the differentiation program.

(define (install-deriv-sum-and-prod)
  (define (deriv-sum operands var)
    (list '+ (map (lambda (x) (deriv x var)) operands)))
  (define (deriv-prod operands var)
    (let ((u (car operands))
          (vs (cdr operands)))
      (if (null? vs)
          (deriv u var)
          (list '+
                (apply list '* (deriv u var) vs)
                (apply list '* u (deriv (cons '* vs) var))))))  
  (put 'deriv '(+) deriv-sum)
  (put 'deriv '(*) deriv-prod))

;; c) Dispatch based differentiation for cos & sin 
(define (install-cos-sin)
  (define (deriv-cos operand var)
    (list '* -1 (list 'sin operand) (deriv operand var)))
  (define (deriv-sin operand var)
    (list '* (list 'cos operand) (deriv operand var)))
  (put 'deriv '(cos) deriv-cos)
  (put 'deriv '(sin) deriv-sin))

;; d) Rather than implementing a deriv operation for a variety of
;; types, one would implement the algebraic operations with a 'deriv
;; dispatch option (and, presumably, an 'eval dispatch option).


;;;; Exos 2.74 (Insatiable Enterprises, Inc.)
;; a) Since employee records are keyed on name in each division (which
;; is a terrible idea, but oh well) we need to provide the name to our
;; `get-record` procedure. Each division will have to provide a
;; package which implements record access for their particular data
;; files. Finally, we'll need to know which division we're accessing
;; in order to dispatch to their package. (assume they'll hard-code
;; their file location into their package)

(define (get-record employee-name division)
  ((get 'get-record division) employee-name))

;; b) Each division must provide an implementation of (get-salary
;; employee-record) to extract their data.

(define (get-salary employee-name division)
  ((get 'get-salary division)
   ((get 'get-record division) employee-name)))

;; c) Someone at headquarters will have to implement (get-division division-file)
;; so that we can dispatch on division type
(define (find-employee-record name division-files)
  (let ((maybe-employee ((get 'get-record (get-division division-file)) name)))
    (if maybe-employee
        maybe-employee
        (if (null? (cdr division-files))
            false
            (find-employee-record name (cdr division-files))))))

;; d) When a new division is created or acquired, it must be
;; integrated into `get-division` at HQ (and any other sites that want
;; to access it) and implement it's own library which implements
;; get-record and get-salary (and get-address, etc.) for their data format.


;;;; Message Passing

;;;; 2.75 message passing make-from-mag-ang

(define (make-from-mag-ang mag ang)
  (define (dispatch op)
    (cond ((eq? op 'magnitude) mag)
          ((eq? op 'angle) ang)
          ((eq? op 'real-part) (* mag (cos ang)))
          ((eq? op 'imag-part) (* mag (sin ang)))
          (else (error "(mag-ang) Unknown operation: " op))))
  dispatch)


;;;; 2.76 Adding new operations & data objects for explicit,
;;   data-directed, and message-passing dispatch.

;; For an explicit dispatch system, adding a new operation requires
;; implementing the explicit function for each data type and handling
;; the object in each existing function (at least those that handle
;; dispatching). Adding a new operation only requires implementing the
;; function for each data type.

;; For a data-directed dispatch system, adding a new operation
;; requires adding implementations for each existing data type and
;; adding them to the dispatch table. Adding a new data type requires
;; implementing each operation for it and adding those entries to the
;; dispatch table.

;; For a message-passing system, adding a new operation requires
;; implementing the operation in each relevant smart-object. Adding a
;; new object type requires implementing a smart object with the
;; relevant operations.

;; In each case, existing code (outside the library implementation)
;; should not have to be altered.

;; For a system where new types are frequently added, the message
;; passing style will be most effective. For a system which frequently
;; adds new operations, the data-directed approach is more appropriate.

;;;; TODO Check this answer ^


;;;; Generic Arithmetic
(define (add x y) (apply-generic 'add x y))
(define (sub x y) (apply-generic 'sub x y))
(define (mul x y) (apply-generic 'mul x y))
(define (div x y) (apply-generic 'div x y))

;; Scheme number package
(define (install-scheme-number-package)
  (define (tag x) (attach-tag 'scheme-number x))
  (put 'add '(scheme-number scheme-number)
       (lambda (x y) (tag (+ x y))))
  (put 'sub '(scheme-number scheme-number)
       (lambda (x y) (tag (- x y))))
  (put 'mul '(scheme-number scheme-number)
       (lambda (x y) (tag (* x y))))
  (put 'div '(scheme-number scheme-number)
       (lambda (x y) (tag (/ x y))))
  (put 'make 'scheme-number (lambda (x) tag x))
  'done)
(define (make-scheme-number n)
  ((get 'make 'scheme-number) n))

;; etc.

;;;; Exos 2.77

;; Louis's expression is fixed because when the new call tries to find
;; the 'complex implementation of magnitude it is redirected to the
;; 'rectangular implementation. The call stack would be (with some elision):

;; (magnitude z)
;; (apply-generic 'magnitude '(complex . '(rectangular . '(3 4))))
;; ((get 'magnitude 'complex) '(rectangular . '(3 4))) ; get generic magnitude
;; ((get 'magnitude 'rectangular) '(3 4)) ; gets magnitude-rectangular
;; (+ (* 3 3) (* 4 4))


;;;; Exos 2.78 Integrated scheme numbers
(define (attach-type-tag type-tag contents)
  (cond
   ((eq? type-tag 'scheme-number) contents)
   (else (cons type-tag contents))))

(define (type-tag obj)
  (cond
   ((number? obj) 'scheme-number)
   ((pair? obj) (car obj))
   (else (error "(type-tag) Bad tagged type: " obj))))

(define (contents obj)
  (cond
   ((number? obj) obj)
   ((pair? obj) (cdr obj))
   (else (error "(contents) Bad tagged type:" obj))))



;;;; Exos 2.79 Generic equality
(define (equ? ob1 ob2)
  ((get 'eql (type-tag ob1) (type-tag ob2)) ob1 ob2))

;; each package must define an eql method, of course.
;; for example:
(define (install-universal-eq)
  (put 'eql '(complex complex) (lambda (a b) (and (= (real-part a) (real-part b))
                                          (= (imag-part a) (imag-part b)))))
  (put 'eql '(rational rational) (lambda a b) (and (= (denom a) (denom b))
                                          (= (numer a) (numer b))))
  (put 'eql '(scheme-number scheme-number) =)
  'done)
;; Not sure how to deal with heterogenous args. I imagine the next
;; section might help.


;;;; Exos 2.80
(define (=zero?)
  ()
  )
(define (install-=zero?)
  (put '=zero? 'scheme-number (lambda (a) (= 0 a)))
  (put '=zero? 'rational (lambda (a) (= 0 (number a))))
  (put '=zero? 'complex (lambda (a) (= 0 (real-part a) (imag-part a))))
  'done)
;; dito on the heterogenous argument types...



;;;; Exos 2.81
;; a) Refering to the implementaiton on p. 265, t1->2, if given two
;;    scheme-number typed arguments:
(define (scheme-number->scheme-number n) n)
(define (complex->complex z) z)
(put-coercion 'scheme-number
              'scheme-number
              scheme-number->scheme-number)
(put-coercion 'complex 'complex complex->complex)

(define (apply-generic op . args)
  (let ((type-tags (map type-tag args)))
    (let ((proc (get op type-tags)))
      (if proc
          (apply proc (map contents args))
          (if (= (length args) 2)
              (let ((type1 (car type-tags))
                    (type2 (cadr type-tags))
                    (a1 (car args))
                    (a2 (cadr args)))
                (let ((t1->t2 (get-coercion type1 type2))
                      (t2->t1 (get-coercion type2 type1)))
                  (cond (t1->t2
                         (apply-generic op (t1->t2 a1) a2))
                        (t2->t1
                         (apply-generic op a1 (t2->t1 a2)))
                        (else (error "No method for these types"
                                     (list op type-tags))))))
              (error "No method for these types"
                     (list op type-tags)))))))
;; - the procedure is not in the type-table, so proc <- (get op
;;   type-tags) will be nil
;;
;; - t1->t2 <- (get-coercien type1 type2) will return schemenum->schemenum
;;
;; - apply-generic will be called on '(op a1 a2) and enter an infinite
;;   recursive loop :(


;; b) As long as the arguments can be coerced to a pair that's in the
;; type-table apply-generic will terminate fine. Otherwise it will
;; throw an error.

;; (internet seems to think that's right)


;; c) New apply generic

(define (apply-generic op . args)
  (let ((type-tags (map type-tag args)))
    (let ((proc (get op type-tags)))
      (if proc
          (apply proc (map contents args))
          (if (= (length args) 2)
              (let ((type1 (car type-tags))
                    (type2 (cadr type-tags))
                    (a1 (car args))
                    (a2 (cadr args)))
                (if (not (= type1 type2))
                    (let ((t1->t2 (get-coercion type1 type2))
                          (t2->t1 (get-coercion type2 type1)))
                      (cond (t1->t2
                             (apply-generic op (t1->t2 a1) a2))
                            (t2->t1
                             (apply-generic op a1 (t2->t1 a2)))
                            (else (error "No method for these types"
                                         (list op type-tags)))))
                    (error "No method for identical types of" type1)))
              (error "No method for these types"
                     (list op type-tags)))))))



;; 2.82 Generalizing apply-generic to multiple arguments

;;  The strategy of trying to coerce all arguments will fail in the
;;  case where the arguments cannot be coerced to the same type but
;;  could all be coerced to a more general type. An example (refering
;;  to the type tree in figure 2.26, because since for every pair of
;;  number types one is a subset of the other no such combination
;;  exists for just numbers) would be an operation defined on polygons
;;  called on a rectangle and a rhombus. Neither argument can be
;;  coerced to the other, but perhaps they could both be coerced to a
;;  parallelogram, trapezoid, etc.

;; After consulting the internet, I see of course that the given
;; implementation will also fail if given two arguments which could be
;; converted to each other but don't have a *direct* converison.

;; A more general strategy would require determining which types each
;; argument *could* be coerced to and then checking those sets for an
;; implementation (possibly the product of those sets, possibly some
;; subset of the product if, for example, the operation is
;; symetric). This is a graph search, requiring some ability to query
;; the type heirarchy for adjacency.

;; We require a few abstract functions (as well as some for convenience):
;; 
;; - a "set" datatype
;; - corce-to (takes a value and anther type and coerces across the
;;   type graph, if it's possible)
;; - coercable-types, which gives the possible coercions for a type
;; - cartesian-product, which applied to m sets returns a list of
;;   length m lists, on for each way the elements of the arguments
;;   could be combined

(define (get-operable op type-tags)
  (let ((operables
         (filter
          (lambda (f) (not (null? f)))
          (map (lambda (types) (car (get op types) types))
               (apply cartesian-product
                      (map coercable-types
                           type-tags))))))
    (if (null? operables)
        (error "No implementation of " op " provided for " type-tags)
        (car operables))))


(define (get-op op type-tags)
)

(define (apply-generic op . args)
  (let ((type-tags (map type-tag args)))
    (let ((opable (get-operable op type-tags)))
      (apply (car opable)
             (map (lambda (source target-t)
                    (coerce-to source target-t))
                  args
                  (cdr opables))))))


;; 2.83 Generic raising operation on a tower of numeric types:
;; - complex
;; - real
;; - rational
;; - integer

(define (raise-integer n)
  (make-rat (contents n) 1))

(define (raise-rational n)
  (make-real (/ (numer n) (denom n))))

(define (raise-real n)
  (make-from-real-image (contents n) 0))

(put 'raise 'int 'raise-integer)
(put 'raise 'rat 'raise-rational)
(put 'raise 'real 'raise-real)


;; 2.84 apply-generic for numeral types

(define (distance-to-top n)
  (define (inner-distance-to-top n i)
    (if (not (null? (get 'raise (type-tag n))))
        (distance-to-top (raise n) (+ i 1))))
  (inner-distance-to-top n))

(define (apply-times f args i)
  (if (= i 0)
      args
      (apply-times f (apply f args) (- i 1))))

(define (get-top-val vals)
  (let ((type-dists (map (lambda (v) (cons (dsitance-to-top (type-tag
                                                             v))
                                           v))
                         vals)))
    (define (get-highest vp vps)
      (cond
       ((null? vps) (cdr vp))
       ((> (car vp) (caar vps)) (get-highest (car vps) (cdr vps)))
       (else (get-highest vp (cdr vps)))))))

(define (raise-to trg src)
  (apply-times
   raise
   (- (distance-to-top src) (distance-to-top trg))))


(define (raiseful-apply-generic op . args)
  (let* ((type-tags (map type-tag args))
         (top-type (get-top-type args))
         (coerced-args (map (lambda (v) (raise-to top-type v)) args)))
    (appy (get op coerced-args) coerced-args)))


;; 2.85 Generic simplification of a numerical tower

;; type-specific `project` operations
(define (project-complex c) (make-real (real c)))
(define (project-real r)
  (define (get-num-denum n d)
    (if (= n (floor n))
        (make-rat n d) ;; leaning on rat simplification
        (get-num denum (* n 10) (* d 10))))
  (get-num-denum r 1))
(define (project-rat r) (make-int (round (/ (numer r) (denum r)))))

(define (generic-drop n)
  (if (apply-generic '= n (apply-generic 'raise (apply-generic 'project n)))
      (generic-drop (apply-generic 'drop n))
      n))

;; creating drop
(put 'drop 'complex 'generic-drop)
(put 'drop 'real 'generic-drop)
(put 'drop 'rat 'generic-drop)

;; Incorporate with "SOTA" apply-generic
(define (simplifying-apply-generic op . args)
  (apply-generic 'drop (raisefully-apply-generic op args)))



;; 2.86 Generic handling of complex numbers
;; pass


;; 2.87 =zero? for polynomials
(define (=zero?-polynomial pnom)
  (define (not= . args)  (not (apply = args)))
  (if (empyt-term-list? pnom)
      #f
      (let* ((term (first-term pnom))
             (c (coeff term))
             (o (order term))
             (terms (rest-terms pnom)))
        (if (not= 0 coeff)
            #f
            (if (empty-term-list? terms)
                #t
                (=zero?-polynomial terms))))))
(put '=zero? 'polynomial '=zero?-polynomial)



;; At this point I've kind of given up on this series of examples :( I
;; can't have any confidence in my solution without an implementation
;; of `put` and `get`.


;; 2.88 Subtraction of polynomials
(define (sub-polynomial p1 p2)
  (apply-generic
   '+
   p1
   (apply-generic '* -1 p2)))

;; /trollface (we defined + and * generically for a reason!)


;; 2.89 Implementation of dense polynomials (dpnoms)

;; Implement as a list of coefficients starting with the 0th order
;; coefficient and increasing towards the end of the list.

(define (adjon-term term terms) (append terms '(term)))
(define (the-empty-termlist) '())
(define (empty-terms? terms) (null? term-s))
(define (get-order order terms) (list-ref terms order))
(define (make-term order coeff) coeff) ;; have to ensure is in right place!!!
;; rest of implementation would be in `add` and `mul` implementations which are
;; straightforward recursion.


;; 2.90 sparse and dense polynomials
;; skip

;; In fact skip the remainder of the chapter :|

