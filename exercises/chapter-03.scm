;;;; Chatper 3: Modularity, Objects, and State


;;;; Exos 3.1 Accumulators

(define (make-accumulator sum)
  (lambda (x)
    (set! sum (+ sum x))
    sum))


;;;; Exos 3.2 Monitored function

(define (make-monitored f)
  (let ((count 0))
    (lambda (s)
      (cond
       ((eq? s 'how-many-calls?) count)
       ((eq? s 'reset-counter) (set! count 0) 0)
       (else (set! count (+ count 1)) (f s))))))

;;;; Exos 3.3 Password Protected Account

(define (make-protected-account balance pword)
  (define (withdraw amount)
    (if (>= balance amount)
        (begin
          (set! balance (- balance amount))
          balance)
        "Insufficient funds"))
  (define (deposit amount)
    (set! balance (+ balance amount))
    balance)
  (define (dispatch p m)
    (if (eq? p pword)
        (cond
         ((eq? m 'withdraw) withdraw)
         ((eq? m 'deposit) deposit)
         (else (error "Unknown message " m)))
        (lambda (x)  "Incorrect password")))
  dispatch)


